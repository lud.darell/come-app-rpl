package com.example.come.come.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout

import com.example.come.come.R
import com.example.come.come.adapter.EventAdapter
import com.example.come.come.model.Promo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_event_history.view.*

class EventHistoryFragment : Fragment() {
    private val db = FirebaseDatabase.getInstance()
    private val auth = FirebaseAuth.getInstance()
    private var dataSet = arrayListOf<Promo>()
    private lateinit var adapter: EventAdapter
    private var currentDate: Long = 0


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_event_history, container, false)

        currentDate = System.currentTimeMillis() / 1000
        getPromo(view)

        adapter = EventAdapter(context!!)
        adapter.dataSet = dataSet
        view.recycler_view_event_history.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        view.recycler_view_event_history.adapter = adapter

        return view
    }

    private fun getPromo(view: View) {
        db.reference.child("Promo").addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(p0: DataSnapshot) {
                dataSet.clear()
                if (p0.exists()) {
                    for (data in p0.children) {
                        var statusMember = false
                        var item = data.getValue(Promo::class.java)
                        if (item != null) {
                            if (item.waktu.get("end")?.toLong()!! < currentDate) {

                                for (member in item.member) {
                                    if (member.equals(auth.currentUser?.uid!!)) {
                                        statusMember = true
                                    }
                                }

                                if (statusMember == true) {
                                    dataSet.add(Promo(data.key.toString(), item.namaProduk, item.kategoriProduk,
                                            item.jenisPromo, item.lokasi, item.waktu, item.jumlahOrang,
                                            item.idPembuat, item.member))
                                }
                            }
                        }
                    }

                    if (dataSet.size > 0) {
                        view.item_null.visibility = LinearLayout.GONE
                        ProfilFragment.EVENT_HISTORY = dataSet.size
                    } else {
                        view.item_null.visibility = LinearLayout.VISIBLE
                    }
                    adapter.notifyDataSetChanged()
                }
            }

        })
    }
}
