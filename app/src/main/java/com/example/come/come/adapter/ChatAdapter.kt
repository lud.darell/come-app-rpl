package com.example.come.come.adapter

import android.R.attr.data
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.come.come.R
import com.example.come.come.model.Chat
import kotlinx.android.synthetic.main.chat_another_user.view.*
import kotlinx.android.synthetic.main.chat_user.view.*
import android.R.attr.name
import com.example.come.come.model.User
import java.text.SimpleDateFormat
import java.util.*


class ChatAdapter(val context: Context, val uid: String) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    var dataSet = arrayListOf<Chat>()
    private val USER = 0
    private val ANOTHER_USER = 1

    override fun onCreateViewHolder(p0: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null
        when (viewType) {
            USER -> viewHolder = ViewHolder1(LayoutInflater.from(p0.context).inflate(R.layout.chat_user, p0, false))
            ANOTHER_USER -> viewHolder = ViewHolder2(LayoutInflater.from(p0.context).inflate(R.layout.chat_another_user, p0, false))
        }
        return viewHolder!!
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            USER -> {
                val viewHolder = holder as ViewHolder1
                configureViewHolder1(viewHolder, position)
            }
            ANOTHER_USER -> {
                val viewHolder = holder as ViewHolder2
                configureViewHolder2(viewHolder, position)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (dataSet.get(position).uid == uid) {
            return USER
        } else {
            return ANOTHER_USER
        }
    }

    private fun configureViewHolder1(holder: ViewHolder1, position: Int) {
        var date = Date(dataSet.get(position).date.toLong()!! * 1000)
        var format = SimpleDateFormat("hh:mm a")

        holder.itemView.text_user.text = dataSet.get(position).message
        Glide.with(context).load(dataSet.get(position).imageUrl).into(holder.itemView.image_user)
        holder.itemView.text_user_time.text = format.format(date)
    }

    private fun configureViewHolder2(holder: ViewHolder2, position: Int) {
        var date = Date(dataSet.get(position).date.toLong()!! * 1000)
        var format = SimpleDateFormat("hh:mm a")

        holder.itemView.text_another_user.text = dataSet.get(position).message
        Glide.with(context).load(dataSet.get(position).imageUrl).into(holder.itemView.image_another_user)
        holder.itemView.text_another_user_time.text = format.format(date)
        holder.itemView.text_another_user_name.text = dataSet.get(position).username
    }

    class ViewHolder1(itemView: View) : RecyclerView.ViewHolder(itemView)
    class ViewHolder2(itemView: View) : RecyclerView.ViewHolder(itemView)
}