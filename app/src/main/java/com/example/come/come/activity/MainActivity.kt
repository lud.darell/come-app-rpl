package com.example.come.come.activity

import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.design.widget.BottomNavigationView
import android.transition.*
import android.view.MenuItem
import android.view.Window
import com.example.come.come.fragment.HomeFragment
import com.example.come.come.fragment.IklanFragment
import com.example.come.come.fragment.ProfilFragment
import com.example.come.come.fragment.PromoFragment
import com.example.come.come.model.BottomNavigationViewHelper
import com.example.come.come.R
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(window) {
            requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)
        }
        setContentView(R.layout.activity_main)

        BottomNavigationViewHelper.disableShiftMode(bottom_navigation)

        supportFragmentManager.beginTransaction().replace(R.id.fragment_frame, HomeFragment()).commit()

        bottom_navigation.setOnNavigationItemSelectedListener(object : BottomNavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                when (item.itemId) {
                    R.id.action_home -> supportFragmentManager.beginTransaction().replace(R.id.fragment_frame, HomeFragment()).commit()
                    R.id.action_iklan -> supportFragmentManager.beginTransaction().replace(R.id.fragment_frame, IklanFragment()).commit()
                    R.id.action_promo -> supportFragmentManager.beginTransaction().replace(R.id.fragment_frame, PromoFragment()).commit()
                    R.id.action_profil -> supportFragmentManager.beginTransaction().replace(R.id.fragment_frame, ProfilFragment()).commit()
                }
                return true
            }
        })
    }
}
