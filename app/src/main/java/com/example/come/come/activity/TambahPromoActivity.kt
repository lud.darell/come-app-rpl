package com.example.come.come.activity

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.example.come.come.R
import kotlinx.android.synthetic.main.activity_tambah_promo.*
import com.google.android.gms.location.places.ui.PlacePicker
import android.content.Intent
import com.google.android.gms.maps.model.LatLng
import android.app.DatePickerDialog
import android.text.TextUtils
import java.util.*
import android.widget.EditText
import java.text.SimpleDateFormat
import android.widget.ArrayAdapter
import android.widget.Toast
import com.example.come.come.model.Promo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase

class TambahPromoActivity : AppCompatActivity(){
    private var PLACE_PICKER_REQUEST = 1
    private var builder = PlacePicker.IntentBuilder()
    private lateinit var location: LatLng
    private val myCalendar = Calendar.getInstance()
    private val db = FirebaseDatabase.getInstance()
    private var start = ""
    private var end = ""
    private val user = FirebaseAuth.getInstance().currentUser

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tambah_promo)

        val adapter = ArrayAdapter.createFromResource(this,
                R.array.kategori_array, R.layout.spinner_item)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinner_kategori.adapter = adapter

        btn_back.setOnClickListener {
            finish()
        }

        text_lokasi.setOnClickListener {
            startActivityForResult(builder.build(this), PLACE_PICKER_REQUEST);
        }

        text_start_promo.setOnClickListener {
            DatePickerDialog(this, date(text_start_promo, "start"), myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show()
        }

        text_end_promo.setOnClickListener {
            DatePickerDialog(this, date(text_end_promo, "end"), myCalendar
                    .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                    myCalendar.get(Calendar.DAY_OF_MONTH)).show()
        }

        btn_add_promo.setOnClickListener {
           addPromo()
        }
    }

    private fun addPromo(){
        if (!TextUtils.isEmpty(text_nama.text) &&
                !TextUtils.isEmpty(spinner_kategori.selectedItem.toString()) &&
                !TextUtils.isEmpty(text_jenis.text) &&
                !TextUtils.isEmpty(text_lokasi.text) &&
                !TextUtils.isEmpty(text_start_promo.text) &&
                !TextUtils.isEmpty(text_end_promo.text) &&
                !TextUtils.isEmpty(text_jumlah_teman.text)){

            val key = db.reference.child("Promo").push().getKey()

            var data = Promo(key!!,text_nama.text.toString(),
                    spinner_kategori.selectedItem.toString(),
                    text_jenis.text.toString(),
                    hashMapOf("latitude" to location.latitude, "longitude" to location.longitude),
                    hashMapOf("start" to start, "end" to end),
                    text_jumlah_teman.text.toString(),
                    user?.uid!!)

            db.reference.child("Promo").child(key!!).setValue(data).addOnCompleteListener {
                if (it.isSuccessful){
                    Toast.makeText(this, "Promo berhasil ditambahkan!", Toast.LENGTH_SHORT).show()
                    finish()
                }else{
                    Toast.makeText(this, "Promo gagal ditambahkan!", Toast.LENGTH_SHORT).show()
                }
            }
        }else{
            Toast.makeText(this, "Data tidak boleh kosong!", Toast.LENGTH_SHORT).show()
        }
    }

    private fun updateLabel(editText: EditText, string: String) {
        val myFormat = "EEE, MMM dd yyyy" //In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        when(string){
            "start" -> start = (myCalendar.timeInMillis/1000).toString()
            "end" -> end = (myCalendar.timeInMillis/1000).toString()
        }
        editText.setText(sdf.format(myCalendar.getTime()))
    }

    private fun date(editText: EditText, string: String): DatePickerDialog.OnDateSetListener {
        return DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            myCalendar.set(Calendar.YEAR, year)
            myCalendar.set(Calendar.MONTH, monthOfYear)
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            updateLabel(editText, string)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when(requestCode){
            PLACE_PICKER_REQUEST -> {
                if (resultCode == Activity.RESULT_OK) {
                    val place = PlacePicker.getPlace(this,data!!)
                    location = place.latLng
                    text_lokasi.setText(place.name)
                }
            }

        }
    }
}
