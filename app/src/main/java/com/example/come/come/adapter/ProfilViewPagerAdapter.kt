package com.example.come.come.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.example.come.come.fragment.EventFragment
import com.example.come.come.fragment.EventHistoryFragment

class ProfilViewPagerAdapter(fm : FragmentManager) : FragmentStatePagerAdapter(fm) {

    override fun getItem(position: Int): Fragment?{
        when(position){
            0 -> return EventFragment()
            1 -> return EventHistoryFragment()
            else -> return null
        }
    }

    override fun getCount(): Int {
        return 2
    }
}
