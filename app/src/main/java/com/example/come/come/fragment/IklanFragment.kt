package com.example.come.come.fragment


import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.transition.Explode
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast

import com.example.come.come.R
import com.example.come.come.adapter.RvIklanAdapter
import com.example.come.come.model.Iklan
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_iklan.view.*


/**
 * A simple [Fragment] subclass.
 *
 */
class IklanFragment : Fragment() {
    private val db = FirebaseDatabase.getInstance()
    private var dataset = arrayListOf<Iklan>()
    private lateinit var adapter: RvIklanAdapter

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_iklan, container, false)

        getDataIklan()

        adapter = RvIklanAdapter(context!!)
        adapter.dataSet = dataset
        view.recycler_view_iklan.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
        view.recycler_view_iklan.adapter = adapter

        return view
    }

    private fun getDataIklan(){
        db.reference.child("Iklan").addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
                Log.w("Error", "loadPost:onCancelled", p0.toException());
            }

            override fun onDataChange(p0: DataSnapshot) {
                dataset.clear()
                for (data in p0.children){
                    var item = data.getValue(Iklan::class.java)
                    if (item != null){
                        dataset.add(item)
                    }
                }
                adapter.notifyDataSetChanged()
            }
        })
    }
}
