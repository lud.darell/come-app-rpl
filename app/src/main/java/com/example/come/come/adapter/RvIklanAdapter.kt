package com.example.come.come.adapter

import android.app.Activity
import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.annotation.RequiresApi
import android.support.v7.widget.RecyclerView
import android.transition.Transition
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.come.come.R
import com.example.come.come.activity.DetailIklanActivity
import com.example.come.come.activity.MainActivity
import com.example.come.come.model.Iklan
import kotlinx.android.synthetic.main.card_iklan.view.*
import java.text.SimpleDateFormat
import java.util.*
import android.util.Pair as UtilPair


class RvIklanAdapter(val context: Context) : RecyclerView.Adapter<RvIklanAdapter.ViewHolder>() {
    var dataSet = arrayListOf<Iklan>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.card_iklan, p0, false)
        val viewHolder = RvIklanAdapter.ViewHolder(view)

        return viewHolder
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onBindViewHolder(holder: ViewHolder, p1: Int) {
        var start = Date(dataSet.get(p1).waktu.get("start")?.toLong()!! * 1000)
        var end = Date(dataSet.get(p1).waktu.get("end")?.toLong()!! * 1000)
        var format = SimpleDateFormat("EEE, MMM dd yyyy")

        holder.itemView.text_ads_name.setText(dataSet.get(p1).nama)
        holder.itemView.text_duration.setText("${format.format(start)} - ${format.format(end)}")
        holder.itemView.text_location.setText(dataSet.get(p1).lokasi)
        Glide.with(this.context).load(dataSet.get(p1).image).into(holder.itemView.image_ads)
        holder.itemView.card_iklan.setOnClickListener {
            val intent = Intent(context, DetailIklanActivity::class.java)
            intent.putExtra(DetailIklanActivity.EXTRA_IKLAN, dataSet.get(p1))

            val options = ActivityOptions.makeSceneTransitionAnimation(
                    context as Activity,
                    UtilPair.create<View,String>(holder.itemView.image_ads!!, "image_ads"),
                    UtilPair.create<View,String>(holder.itemView.text_ads_name, "text_ads_name"),
                    UtilPair.create<View,String>(holder.itemView.text_duration, "text_duration"),
                    UtilPair.create<View,String>(holder.itemView.image_location, "image_location"),
                    UtilPair.create<View,String>(holder.itemView.text_location, "text_location")
            )

            context.startActivity(intent, options.toBundle())
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}