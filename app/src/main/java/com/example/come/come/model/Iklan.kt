package com.example.come.come.model

import java.io.Serializable

data class Iklan(val deskripsi: String = "",
                 val image : String = "",
                 val kategori: String = "",
                 val ketentuan: String = "",
                 val lokasi: String = "",
                 val nama : String = "",
                 val pembuat: HashMap<String,String> = hashMapOf(),
                 val waktu : HashMap<String,String> = hashMapOf()) : Serializable