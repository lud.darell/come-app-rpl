package com.example.come.come.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout

import com.example.come.come.R
import com.example.come.come.adapter.EventAdapter
import com.example.come.come.model.Promo
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_event.view.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 *
 */
class EventFragment : Fragment() {
    private val db = FirebaseDatabase.getInstance()
    private val auth = FirebaseAuth.getInstance()
    private var dataSet = arrayListOf<Promo>()
    private lateinit var adapter: EventAdapter
    private var currentDate: Long = 0

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        var view = inflater.inflate(R.layout.fragment_event, container, false)

        currentDate = System.currentTimeMillis() / 1000
        getPromo(view)

        adapter = EventAdapter(context!!)
        adapter.dataSet = dataSet
        view.recycler_view_event.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        view.recycler_view_event.adapter = adapter

        return view
    }

    private fun getPromo(view: View) {
        db.reference.child("Promo").addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(p0: DataSnapshot) {
                dataSet.clear()
                if (p0.exists()) {
                    for (data in p0.children) {
                        var statusMember = false
                        var item = data.getValue(Promo::class.java)
                        if (item != null) {
                            if (item.waktu.get("end")?.toLong()!! >= currentDate) {

                                for (member in item.member) {
                                    if (member.equals(auth.currentUser?.uid!!)) {
                                        statusMember = true
                                    }
                                }

                                if (statusMember == true) {
                                    dataSet.add(Promo(data.key.toString(), item.namaProduk, item.kategoriProduk,
                                            item.jenisPromo, item.lokasi, item.waktu, item.jumlahOrang,
                                            item.idPembuat, item.member))
                                }
                            }
                        }
                    }

                    if (dataSet.size > 0) {
                        view.item_null.visibility = LinearLayout.GONE
                        ProfilFragment.EVENT = dataSet.size
                    } else {
                        view.item_null.visibility = LinearLayout.VISIBLE
                    }
                    adapter.notifyDataSetChanged()
                }
            }

        })
    }
}
