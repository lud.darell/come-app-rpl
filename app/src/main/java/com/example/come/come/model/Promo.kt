package com.example.come.come.model

import java.io.Serializable

data class Promo(val idPromo: String = "",
                 val namaProduk: String = "",
                 val kategoriProduk: String = "",
                 val jenisPromo: String = "",
                 val lokasi: HashMap<String, Double> = hashMapOf(),
                 val waktu: HashMap<String, String> = hashMapOf(),
                 val jumlahOrang: String = "",
                 val idPembuat: String = "",
                 val member: ArrayList<String> = arrayListOf()) : Serializable