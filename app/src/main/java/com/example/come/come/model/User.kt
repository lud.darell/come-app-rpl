package com.example.come.come.model

import java.io.Serializable

data class User(val uid: String = "",
                val email: String = "",
                val name: String = "",
                val imageUrl: String = "") : Serializable