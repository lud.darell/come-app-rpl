package com.example.come.come.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.MenuItem
import com.example.come.come.R
import com.example.come.come.adapter.ChatAdapter
import com.example.come.come.model.Chat
import com.example.come.come.model.Promo
import com.example.come.come.model.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.activity_chat.*

class ChatActivity : AppCompatActivity() {
    private val db = FirebaseDatabase.getInstance()
    private val auth = FirebaseAuth.getInstance()
    private lateinit var adapter: ChatAdapter
    private var dataSet = arrayListOf<Chat>()
    private var user = arrayListOf<User>()
    private lateinit var extraPromo: Promo

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)

        extraPromo = intent.getSerializableExtra("PROMO") as Promo

        supportActionBar?.title = extraPromo.namaProduk
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        getUser()

        btn_send.setOnClickListener {
            addChat()
        }

        adapter = ChatAdapter(this, auth?.currentUser?.uid!!)
        adapter.dataSet = dataSet
        recycler_view_chat.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        recycler_view_chat.scrollToPosition(dataSet.size-1)
        recycler_view_chat.adapter = adapter
    }

    private fun getChat() {
        if (user.size > 0) {
            db.reference.child("Group").child(extraPromo.idPromo).addValueEventListener(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun onDataChange(p0: DataSnapshot) {
                    dataSet.clear()
                    if (p0.exists()) {
                        for (data in p0.children) {
                            var chat = data.getValue(Chat::class.java)
                            if (chat != null) {
                                Log.d("CekUser", "Hello")
                                for (dataUser in user) {
                                    if (dataUser.uid.equals(chat.uid)) {
                                        dataSet.add(Chat(chat.date, chat.uid, chat.message, dataUser.name, dataUser.imageUrl))
                                    }
                                }
                            }
                        }
                    }
                    adapter.notifyDataSetChanged()
                    recycler_view_chat.scrollToPosition(dataSet.size-1)
                }
            })
        }
    }

    private fun addChat() {
        val key = db.reference.child("Group").child(extraPromo.idPromo).push().getKey()
        if (text_chat.text != null) {
            var chat = mapOf(
                    "date" to (System.currentTimeMillis() / 1000).toString(),
                    "uid" to auth?.currentUser?.uid,
                    "message" to text_chat.text.toString())

            if (key != null) {
                db.reference.child("Group").child(extraPromo.idPromo).child(key).setValue(chat).addOnCompleteListener {
                    if (it.isSuccessful) {
                        text_chat.text = null
                    }
                }
            }
        }
    }

    private fun getUser() {
        user.clear()
        for (uid in extraPromo.member) {
            db.reference.child("User").child(uid).addValueEventListener(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {
                    TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
                }

                override fun onDataChange(p0: DataSnapshot) {
                    var data = p0.getValue(User::class.java)
                    Log.d("FromUser", data?.name)
                    if (data != null) {
                        user.add(User(p0.key!!, data.email, data.name, data.imageUrl))
                    }
                    getChat()
                }
            })
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
}
