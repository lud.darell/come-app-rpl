package com.example.come.come.fragment

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentActivity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.example.come.come.adapter.ProfilViewPagerAdapter

import com.example.come.come.R
import com.example.come.come.activity.LoginActivity
import com.example.come.come.model.User
import com.google.android.gms.dynamic.SupportFragmentWrapper
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_profil.view.*

class ProfilFragment : Fragment() {
    private val auth = FirebaseAuth.getInstance()
    private val db = FirebaseDatabase.getInstance()
    private var user: User? = null

    companion object {
        var EVENT = 0
        var EVENT_HISTORY = 0
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_profil, container, false)

        getUser(view)

        view.btn_sign_out.setOnClickListener {
            auth.signOut()
            val intent = Intent(activity, LoginActivity::class.java)
            startActivity(intent)
            activity?.finish()
        }

        view.tab_layout.addTab(view.tab_layout.newTab().setText("${EVENT} Event"))
        view.tab_layout.addTab(view.tab_layout.newTab().setText("${EVENT_HISTORY} History Event"))

        view.view_pager.adapter = ProfilViewPagerAdapter(childFragmentManager)
        view.view_pager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(view.tab_layout))
        view.tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab != null) {
                    view.view_pager.currentItem = tab.position
                }
            }

        })

        return view
    }

    private fun getUser(view: View) {
        db.reference.child("User").child(auth?.currentUser?.uid!!).addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()) {
                    user = p0.getValue(User::class.java)
                    if (user != null) {
                        view.text_name.setText(user?.name)
                        Glide.with(activity!!).load(user?.imageUrl).into(view.photo)
                    }
                }
            }

        })
    }
}
