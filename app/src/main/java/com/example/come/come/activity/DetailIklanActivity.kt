package com.example.come.come.activity

import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.view.MenuItem
import android.view.Window
import com.bumptech.glide.Glide
import com.example.come.come.R
import com.example.come.come.model.Iklan
import kotlinx.android.synthetic.main.activity_detail_iklan.*
import java.text.SimpleDateFormat
import java.util.*

class DetailIklanActivity : AppCompatActivity() {
    companion object {
        const val EXTRA_IKLAN = "IKLAN"
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(window) {
            requestFeature(Window.FEATURE_CONTENT_TRANSITIONS)
        }
        setContentView(R.layout.activity_detail_iklan)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.title = ""

        val iklan = intent.getSerializableExtra(EXTRA_IKLAN) as Iklan
        var start = Date(iklan.waktu.get("start")?.toLong()!! * 1000)
        var end = Date(iklan.waktu.get("end")?.toLong()!! * 1000)
        var format = SimpleDateFormat("EEE, MMM dd yyyy")

        Glide.with(this).load(iklan.image).into(image_ads)
        text_ads_name.setText(iklan.nama)
        text_duration.setText("${format.format(start)} - ${format.format(end)}")
        text_location.setText(iklan.lokasi)
        text_kategori.setText(iklan.kategori)
        text_deskripsi.setText(iklan.deskripsi)
        text_syarat.setText(iklan.ketentuan)
        text_pembuat.setText(iklan.pembuat.get("nama"))
        Glide.with(this).load(iklan.pembuat.get("image")).into(image_pembuat)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.getItemId() === android.R.id.home) {
            supportFinishAfterTransition()
        }

        return super.onOptionsItemSelected(item)
    }
}
