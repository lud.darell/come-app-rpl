package com.example.come.come.activity

import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.widget.RelativeLayout
import android.widget.Toast
import com.example.come.come.R
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.UserProfileChangeRequest
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.activity_register.*

class RegisterActivity : AppCompatActivity() {
    private val auth = FirebaseAuth.getInstance()
    private val db = FirebaseDatabase.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        btn_sign_up.setOnClickListener {
            signUp()
        }

        btn_sign_in.setOnClickListener {
            finish()
        }
    }

    fun signUp() {
        if (!TextUtils.isEmpty(text_full_name.text) && !TextUtils.isEmpty(text_email.text) && !TextUtils.isEmpty(text_password.text)) {
            loading.visibility = RelativeLayout.VISIBLE
            auth.createUserWithEmailAndPassword(text_email.text.toString(), text_password.text.toString()).addOnCompleteListener {
                if (it.isSuccessful){
                    val user = mapOf(
                            "name" to text_full_name.text.toString(),
                            "email" to text_email.text.toString(),
                            "imageUrl" to "https://firebasestorage.googleapis.com/v0/b/come-174e1.appspot.com/o/Default%20Data%2Fdefault_photo.png?alt=media&token=17820318-148b-4231-9d87-3fc83b9fa58d"
                    )

                    db.reference.child("User").child(auth.currentUser?.uid!!).setValue(user).addOnCompleteListener {
                        if (it.isSuccessful){
                            finish()
                        }
                    }
                }
            }.addOnFailureListener {
                Toast.makeText(this@RegisterActivity, it.message, Toast.LENGTH_SHORT).show()
                loading.visibility = RelativeLayout.GONE
            }
        }
    }
}
