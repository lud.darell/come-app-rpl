package com.example.come.come.fragment


import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.example.come.come.R
import com.example.come.come.activity.DetailPromoActivity
import com.example.come.come.activity.TambahPromoActivity
import com.example.come.come.model.Promo
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_home.view.*

class HomeFragment : Fragment(), OnMapReadyCallback {
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private val REQUEST_PERMISSION_LOCATION = 9001
    private lateinit var latLng: LatLng
    private val db = FirebaseDatabase.getInstance()
    private var promo = arrayListOf<Promo>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(activity!!)

        val supportMapFragment = childFragmentManager?.findFragmentById(R.id.view_map) as SupportMapFragment?
        supportMapFragment?.getMapAsync(this)

        view.btn_add.setOnClickListener {
            val intent = Intent(activity, TambahPromoActivity::class.java)
            activity?.startActivity(intent)
        }


        return view
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        googleMap?.uiSettings?.isMapToolbarEnabled = false
        updateLocation(googleMap)
        getPromo(googleMap)
    }

    private fun updateLocation(googleMap: GoogleMap?) {
        if (ActivityCompat.checkSelfPermission(context!!, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity!!, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION), REQUEST_PERMISSION_LOCATION)
        }
        fusedLocationProviderClient.lastLocation.addOnSuccessListener { location ->
            if (location != null) {
                latLng = LatLng(location.latitude, location.longitude)
                googleMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))
            }
        }
        googleMap?.isMyLocationEnabled = true
    }

    private fun getPromo(googleMap: GoogleMap?) {
        db.reference.child("Promo").addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
            }

            override fun onDataChange(p0: DataSnapshot) {
                promo.clear()
                for (data in p0.children) {
                    var item = data.getValue(Promo::class.java)
                    if (item != null) {
                        promo.add(item)
                    }
                }
                addMarker(googleMap)
            }
        })
    }

    private fun addMarker(googleMap: GoogleMap?) {
        if (googleMap != null) {
            googleMap.clear()
            for (data in promo)
                googleMap.addMarker(MarkerOptions().position(LatLng(data.lokasi.get("latitude")!!,
                        data.lokasi.get("longitude")!!))
                        .title(data.namaProduk))
                        .setIcon(BitmapDescriptorFactory.fromResource(R.mipmap.ic_marker))
        }

        googleMap?.setOnInfoWindowClickListener {
            for (data in promo) {
                if (data.namaProduk.equals(it.title) &&
                        data.lokasi.get("latitude") == it.position.latitude &&
                        data.lokasi.get("longitude") == it.position.longitude) {
                    val intent = Intent(context, DetailPromoActivity::class.java)
                    intent.putExtra("PROMO", data)
                    startActivity(intent)
                }
            }
        }
    }
}
