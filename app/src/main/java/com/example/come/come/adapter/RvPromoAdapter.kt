package com.example.come.come.adapter

import android.app.Activity
import android.app.ActivityOptions
import android.content.Context
import android.content.Intent
import android.location.Address
import android.location.Geocoder
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.come.come.R
import com.example.come.come.activity.DetailPromoActivity
import com.example.come.come.model.Promo
import com.google.android.gms.maps.model.LatLng
import kotlinx.android.synthetic.main.card_promo.view.*
import java.text.SimpleDateFormat
import java.util.*
import android.util.Pair as UtilPair

class RvPromoAdapter(val context: Context): RecyclerView.Adapter<RvPromoAdapter.ViewHolder>() {
    var dataSet = arrayListOf<Promo>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val view = LayoutInflater.from(p0.context).inflate(R.layout.card_promo, p0, false)
        val viewHolder = RvPromoAdapter.ViewHolder(view)

        return viewHolder
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.text_nama.setText(dataSet.get(position).namaProduk)

        var location = getAddress(
                LatLng(dataSet.get(position).lokasi.get("latitude")!!,
                        dataSet.get(position).lokasi.get("longitude")!!))
        holder.itemView.text_lokasi.setText(location.get(0).getAddressLine(0))

        holder.itemView.text_jenis.setText(dataSet.get(position).jenisPromo)

        var start = Date(dataSet.get(position).waktu.get("start")?.toLong()!! * 1000)
        var end = Date(dataSet.get(position).waktu.get("end")?.toLong()!! * 1000)
        var format = SimpleDateFormat("EEE, MMM dd yyyy")
        holder.itemView.text_durasi.setText("${format.format(start)}\n${format.format(end)}")

        holder.itemView.card_promo.setOnClickListener {
            val intent = Intent(context, DetailPromoActivity::class.java)
            intent.putExtra(DetailPromoActivity.EXTRA_PROMO, dataSet.get(position))
            context.startActivity(intent)
        }
    }

    private fun getAddress(latLng: LatLng): List<Address> {
        try {
            var geocoder = Geocoder(context, Locale.getDefault())
            return geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
        }catch (e: java.lang.Exception){
            return listOf()
        }
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)
}