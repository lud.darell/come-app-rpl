package com.example.come.come.activity

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Address
import android.location.Geocoder
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.come.come.R
import com.example.come.come.model.Promo
import kotlinx.android.synthetic.main.activity_detail_promo.*
import android.util.Log
import android.view.MenuItem
import com.bumptech.glide.Glide
import com.example.come.come.model.User
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import java.text.SimpleDateFormat
import java.util.*


class DetailPromoActivity : AppCompatActivity() {
    private lateinit var extraPromo: Promo
    private val db = FirebaseDatabase.getInstance()
    private val auth = FirebaseAuth.getInstance()
    private lateinit var user : User

    companion object {
        const val EXTRA_PROMO = "PROMO"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_promo)

        extraPromo = intent.getSerializableExtra(EXTRA_PROMO) as Promo
        supportActionBar?.title = "Detail Promo"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        getUser()
        text_promo_name.text = extraPromo.namaProduk
        text_kategori.text = extraPromo.kategoriProduk
        text_jumlah_teman.text = "${extraPromo.jumlahOrang} Orang"

        var start = Date(extraPromo.waktu.get("start")?.toLong()!! * 1000)
        var end = Date(extraPromo.waktu.get("end")?.toLong()!! * 1000)
        var format = SimpleDateFormat("EEE, MMM dd yyyy")
        text_duration.text = "${format.format(start)} - ${format.format(end)}"

        var location = getAddress(
                LatLng(extraPromo.lokasi.get("latitude")!!,
                        extraPromo.lokasi.get("longitude")!!))
        text_location.text = location.get(0).getAddressLine(0)

        text_jenis.text = extraPromo.jenisPromo

        btn_join.setOnClickListener {
            joinPromo()
        }
    }

    private fun getUser(){
        db.reference.child("User").child(extraPromo.idPembuat).addValueEventListener(object : ValueEventListener{
            override fun onCancelled(p0: DatabaseError) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onDataChange(p0: DataSnapshot) {
                if (p0.exists()){
                    user = p0.getValue(User::class.java)!!
                }

                Glide.with(this@DetailPromoActivity).load(user.imageUrl).into(image_pembuat)
                text_pembuat.text = user.name
            }
        })
    }

    private fun getAddress(latLng: LatLng): List<Address> {
        try {
            var geocoder = Geocoder(this, Locale.getDefault())
            return geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
        }catch (e: java.lang.Exception){
            return listOf()
        }
    }

    private fun joinPromo() {
        try {
            if (extraPromo.member.size <= extraPromo.jumlahOrang.toInt()) {
                var member = extraPromo.member
                member.add(auth.currentUser?.uid!!)
                db.reference.child("Promo").child(extraPromo.idPromo)
                        .updateChildren(mapOf("member" to member)).addOnCompleteListener {
                            if (it.isSuccessful) {
                                Toast.makeText(this, "Join Success", Toast.LENGTH_SHORT).show()
                                finish()
                            }
                        }.addOnFailureListener {
                            Toast.makeText(this, it.message, Toast.LENGTH_SHORT).show()
                        }
            } else {
                Toast.makeText(this, "Kuota Penuh", Toast.LENGTH_SHORT).show()
            }
        } catch (e: Exception) {
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.getItemId()) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }
}
