package com.example.come.come.fragment


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast

import com.example.come.come.R
import com.example.come.come.adapter.RvPromoAdapter
import com.example.come.come.model.Promo
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.android.synthetic.main.fragment_promo.view.*

class PromoFragment : Fragment() {
    private lateinit var adapter: RvPromoAdapter
    private var dataSet = arrayListOf<Promo>()
    private var db = FirebaseDatabase.getInstance()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_promo, container, false)

        getPromo()

        adapter = RvPromoAdapter(context!!)
        adapter.dataSet = dataSet
        view.recycler_view_promo.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        view.recycler_view_promo.adapter = adapter

        return view
    }

    private fun getPromo() {
        db.reference.child("Promo").addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Toast.makeText(context, p0.message, Toast.LENGTH_SHORT).show()
            }

            override fun onDataChange(p0: DataSnapshot) {
                dataSet.clear()
                for (data in p0.children) {
                    var item = data.getValue(Promo::class.java)
                    if (item != null) {
                        dataSet.add(Promo(data.key.toString(), item.namaProduk, item.kategoriProduk,
                                item.jenisPromo, item.lokasi, item.waktu, item.jumlahOrang,
                                item.idPembuat, item.member))
                    }
                }
                adapter.notifyDataSetChanged()
            }
        })
    }
}
