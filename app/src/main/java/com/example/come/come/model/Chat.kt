package com.example.come.come.model

data class Chat(val date: String = "",
                val uid: String = "",
                val message: String = "",
                val username: String = "",
                val imageUrl: String = "")